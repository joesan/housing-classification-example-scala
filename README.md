# housing-classification-example-scala

@deprecated

[![Build](https://github.com/joesan/housing-classification-example-scala/actions/workflows/main.yml/badge.svg)](https://github.com/joesan/housing-classification-example-scala/actions/workflows/main.yml)

A data preparation module for the housing classification data science problem!

## Pre-Requisites

First, we need to have Python3 installed which I hope you have it already. Next get the Jupyter installed as below:

```
python3 -m pip install --upgrade pip
python3 -m pip install jupyter
```

Next, we will install the Scala kernel for Jupyter which will be the [Almond project](https://almond.sh/). For this we first
have to [install Ammonite](https://ammonite.io/#InstallationonLinux). Once Ammonite is installed, proceed with the steps below.

As a next step, we need to have a Scala kernel for the Jupyter notebook for which I'm using [Almond](https://almond.sh/). For a local
installation, follow the steps as below (I'm on Ubuntu 20.04):

```
yourname@yourcomputer:~$ curl -Lo coursier https://git.io/coursier-cli
yourname@yourcomputer~:~$ chmod +x coursier
yourname@yourcomputer~:~$ ./coursier launch --fork almond -M almond.ScalaKernel -- --install
yourname@yourcomputer~:~$ rm -f coursier
```

To install a specific Scala version, use the following command:

```
./coursier launch --fork almond:0.10.0 --scala 2.12.11 -- --install --force
```

TODO.... https://queirozf.com/entries/jupyter-kernels-how-to-add-change-remove#add-scala-kernel
